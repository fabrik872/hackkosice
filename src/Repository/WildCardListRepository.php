<?php

namespace App\Repository;

use App\Entity\WildCardList;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method WildCardList|null find($id, $lockMode = null, $lockVersion = null)
 * @method WildCardList|null findOneBy(array $criteria, array $orderBy = null)
 * @method WildCardList[]    findAll()
 * @method WildCardList[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WildCardListRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, WildCardList::class);
    }

    // /**
    //  * @return WildCardList[] Returns an array of WildCardList objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?WildCardList
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
