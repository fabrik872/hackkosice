<?php

namespace App\Repository;

use App\Entity\WildCard;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method WildCard|null find($id, $lockMode = null, $lockVersion = null)
 * @method WildCard|null findOneBy(array $criteria, array $orderBy = null)
 * @method WildCard[]    findAll()
 * @method WildCard[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WildCardRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, WildCard::class);
    }

    // /**
    //  * @return WildCard[] Returns an array of WildCard objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?WildCard
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
