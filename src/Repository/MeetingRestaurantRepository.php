<?php

namespace App\Repository;

use App\Entity\MeetingRestaurant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method MeetingRestaurant|null find($id, $lockMode = null, $lockVersion = null)
 * @method MeetingRestaurant|null findOneBy(array $criteria, array $orderBy = null)
 * @method MeetingRestaurant[]    findAll()
 * @method MeetingRestaurant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MeetingRestaurantRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, MeetingRestaurant::class);
    }

    // /**
    //  * @return MeetingRestaurant[] Returns an array of MeetingRestaurant objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MeetingRestaurant
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
