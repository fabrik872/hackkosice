<?php

namespace App\Repository;

use App\Entity\MeetingUsers;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method MeetingUsers|null find($id, $lockMode = null, $lockVersion = null)
 * @method MeetingUsers|null findOneBy(array $criteria, array $orderBy = null)
 * @method MeetingUsers[]    findAll()
 * @method MeetingUsers[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MeetingUsersRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, MeetingUsers::class);
    }

    // /**
    //  * @return MeetingUsers[] Returns an array of MeetingUsers objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MeetingUsers
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
