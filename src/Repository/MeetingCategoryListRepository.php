<?php


namespace App\Repository;

use App\Entity\MeetingCategoryList;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method MeetingCategoryList|null find($id, $lockMode = null, $lockVersion = null)
 * @method MeetingCategoryList|null findOneBy(array $criteria, array $orderBy = null)
 * @method MeetingCategoryList[]    findAll()
 * @method MeetingCategoryList[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MeetingCategoryListRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, MeetingCategoryList::class);
    }
}