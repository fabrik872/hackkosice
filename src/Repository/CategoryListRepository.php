<?php

namespace App\Repository;

use App\Entity\CategoryList;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CategoryList|null find($id, $lockMode = null, $lockVersion = null)
 * @method CategoryList|null findOneBy(array $criteria, array $orderBy = null)
 * @method CategoryList[]    findAll()
 * @method CategoryList[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryListRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CategoryList::class);
    }

    // /**
    //  * @return CategoryList[] Returns an array of CategoryList objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CategoryList
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
