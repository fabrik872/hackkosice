<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190331013705 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE category (id INTEGER NOT NULL, restaurant_id_id INTEGER NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_64C19C135592D86 ON category (restaurant_id_id)');
        $this->addSql('CREATE TABLE category_category_list (category_id INTEGER NOT NULL, category_list_id INTEGER NOT NULL, PRIMARY KEY(category_id, category_list_id))');
        $this->addSql('CREATE INDEX IDX_D188A71412469DE2 ON category_category_list (category_id)');
        $this->addSql('CREATE INDEX IDX_D188A714FA7656F5 ON category_category_list (category_list_id)');
        $this->addSql('CREATE TABLE category_list (id INTEGER NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE restaurant (id INTEGER NOT NULL, name VARCHAR(255) NOT NULL, description CLOB DEFAULT NULL, image VARCHAR(255) NOT NULL, url VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE wild_card (id INTEGER NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE wild_card_restaurant (wild_card_id INTEGER NOT NULL, restaurant_id INTEGER NOT NULL, PRIMARY KEY(wild_card_id, restaurant_id))');
        $this->addSql('CREATE INDEX IDX_79C483D4916C96CB ON wild_card_restaurant (wild_card_id)');
        $this->addSql('CREATE INDEX IDX_79C483D4B1E7706E ON wild_card_restaurant (restaurant_id)');
        $this->addSql('CREATE TABLE wild_card_wild_card_list (wild_card_id INTEGER NOT NULL, wild_card_list_id INTEGER NOT NULL, PRIMARY KEY(wild_card_id, wild_card_list_id))');
        $this->addSql('CREATE INDEX IDX_5CC9CD95916C96CB ON wild_card_wild_card_list (wild_card_id)');
        $this->addSql('CREATE INDEX IDX_5CC9CD95C4D2943F ON wild_card_wild_card_list (wild_card_list_id)');
        $this->addSql('CREATE TABLE wild_card_list (id INTEGER NOT NULL, name VARCHAR(255) NOT NULL, weight INTEGER NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE meeting (id INTEGER NOT NULL, date DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE meeting_category_list (id INTEGER NOT NULL, ranking INTEGER NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE meeting_category_list_meeting_users (meeting_category_list_id INTEGER NOT NULL, meeting_users_id INTEGER NOT NULL, PRIMARY KEY(meeting_category_list_id, meeting_users_id))');
        $this->addSql('CREATE INDEX IDX_F6AA13E75A7DEEF7 ON meeting_category_list_meeting_users (meeting_category_list_id)');
        $this->addSql('CREATE INDEX IDX_F6AA13E7F42299E8 ON meeting_category_list_meeting_users (meeting_users_id)');
        $this->addSql('CREATE TABLE meeting_category_list_category_list (meeting_category_list_id INTEGER NOT NULL, category_list_id INTEGER NOT NULL, PRIMARY KEY(meeting_category_list_id, category_list_id))');
        $this->addSql('CREATE INDEX IDX_454B76515A7DEEF7 ON meeting_category_list_category_list (meeting_category_list_id)');
        $this->addSql('CREATE INDEX IDX_454B7651FA7656F5 ON meeting_category_list_category_list (category_list_id)');
        $this->addSql('CREATE TABLE meeting_restaurant (id INTEGER NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE meeting_restaurant_meeting_users (meeting_restaurant_id INTEGER NOT NULL, meeting_users_id INTEGER NOT NULL, PRIMARY KEY(meeting_restaurant_id, meeting_users_id))');
        $this->addSql('CREATE INDEX IDX_41CCFC14417FCBA ON meeting_restaurant_meeting_users (meeting_restaurant_id)');
        $this->addSql('CREATE INDEX IDX_41CCFC14F42299E8 ON meeting_restaurant_meeting_users (meeting_users_id)');
        $this->addSql('CREATE TABLE meeting_restaurant_restaurant (meeting_restaurant_id INTEGER NOT NULL, restaurant_id INTEGER NOT NULL, PRIMARY KEY(meeting_restaurant_id, restaurant_id))');
        $this->addSql('CREATE INDEX IDX_4EFB5413417FCBA ON meeting_restaurant_restaurant (meeting_restaurant_id)');
        $this->addSql('CREATE INDEX IDX_4EFB5413B1E7706E ON meeting_restaurant_restaurant (restaurant_id)');
        $this->addSql('CREATE TABLE meeting_users (id INTEGER NOT NULL, meeting_id_id INTEGER NOT NULL, user_id_id INTEGER NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_7CB2E7E91775DC57 ON meeting_users (meeting_id_id)');
        $this->addSql('CREATE INDEX IDX_7CB2E7E99D86650F ON meeting_users (user_id_id)');
        $this->addSql('CREATE TABLE user (id INTEGER NOT NULL, email VARCHAR(255) NOT NULL, name VARCHAR(40) NOT NULL, role VARCHAR(50) NOT NULL, password VARCHAR(64) NOT NULL, PRIMARY KEY(id))');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE category_category_list');
        $this->addSql('DROP TABLE category_list');
        $this->addSql('DROP TABLE restaurant');
        $this->addSql('DROP TABLE wild_card');
        $this->addSql('DROP TABLE wild_card_restaurant');
        $this->addSql('DROP TABLE wild_card_wild_card_list');
        $this->addSql('DROP TABLE wild_card_list');
        $this->addSql('DROP TABLE meeting');
        $this->addSql('DROP TABLE meeting_category_list');
        $this->addSql('DROP TABLE meeting_category_list_meeting_users');
        $this->addSql('DROP TABLE meeting_category_list_category_list');
        $this->addSql('DROP TABLE meeting_restaurant');
        $this->addSql('DROP TABLE meeting_restaurant_meeting_users');
        $this->addSql('DROP TABLE meeting_restaurant_restaurant');
        $this->addSql('DROP TABLE meeting_users');
        $this->addSql('DROP TABLE user');
    }
}
