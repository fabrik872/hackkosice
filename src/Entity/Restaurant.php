<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RestaurantRepository")
 */
class Restaurant
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Category", mappedBy="restaurantId", orphanRemoval=true, fetch="EAGER")
     */
    private $categories;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\WildCard", mappedBy="restaurantId", fetch="EAGER")
     */
    private $wildCards;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\MeetingRestaurant", mappedBy="restaurantId", fetch="EAGER")
     */
    private $meetingRestaurants;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $url;

    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->wildCards = new ArrayCollection();
        $this->meetingRestaurants = new ArrayCollection();
    }

    public function __toString(): ?string
    {
        return $this->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
            $category->setRestaurantId($this);
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        if ($this->categories->contains($category)) {
            $this->categories->removeElement($category);
            // set the owning side to null (unless already changed)
            if ($category->getRestaurantId() === $this) {
                $category->setRestaurantId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|WildCard[]
     */
    public function getWildCards(): Collection
    {
        return $this->wildCards;
    }

    public function addWildCard(WildCard $wildCard): self
    {
        if (!$this->wildCards->contains($wildCard)) {
            $this->wildCards[] = $wildCard;
            $wildCard->addRestaurantId($this);
        }

        return $this;
    }

    public function removeWildCard(WildCard $wildCard): self
    {
        if ($this->wildCards->contains($wildCard)) {
            $this->wildCards->removeElement($wildCard);
            $wildCard->removeRestaurantId($this);
        }

        return $this;
    }

    /**
     * @return Collection|MeetingRestaurant[]
     */
    public function getMeetingRestaurants(): Collection
    {
        return $this->meetingRestaurants;
    }

    public function addMeetingRestaurant(MeetingRestaurant $meetingRestaurant): self
    {
        if (!$this->meetingRestaurants->contains($meetingRestaurant)) {
            $this->meetingRestaurants[] = $meetingRestaurant;
            $meetingRestaurant->addRestaurantId($this);
        }

        return $this;
    }

    public function removeMeetingRestaurant(MeetingRestaurant $meetingRestaurant): self
    {
        if ($this->meetingRestaurants->contains($meetingRestaurant)) {
            $this->meetingRestaurants->removeElement($meetingRestaurant);
            $meetingRestaurant->removeRestaurantId($this);
        }

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }
}
