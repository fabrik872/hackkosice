<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\WildCardRepository")
 */
class WildCard
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Restaurant", inversedBy="wildCards")
     */
    private $restaurantId;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\WildCardList", inversedBy="wildCards")
     */
    private $wildCardListId;

    public function __construct()
    {
        $this->restaurantId = new ArrayCollection();
        $this->wildCardListId = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Restaurant[]
     */
    public function getRestaurantId(): Collection
    {
        return $this->restaurantId;
    }

    public function addRestaurantId(Restaurant $restaurantId): self
    {
        if (!$this->restaurantId->contains($restaurantId)) {
            $this->restaurantId[] = $restaurantId;
        }

        return $this;
    }

    public function removeRestaurantId(Restaurant $restaurantId): self
    {
        if ($this->restaurantId->contains($restaurantId)) {
            $this->restaurantId->removeElement($restaurantId);
        }

        return $this;
    }

    /**
     * @return Collection|WildCardList[]
     */
    public function getWildCardListId(): Collection
    {
        return $this->wildCardListId;
    }

    public function addWildCardListId(WildCardList $wildCardListId): self
    {
        if (!$this->wildCardListId->contains($wildCardListId)) {
            $this->wildCardListId[] = $wildCardListId;
        }

        return $this;
    }

    public function removeWildCardListId(WildCardList $wildCardListId): self
    {
        if ($this->wildCardListId->contains($wildCardListId)) {
            $this->wildCardListId->removeElement($wildCardListId);
        }

        return $this;
    }
}
