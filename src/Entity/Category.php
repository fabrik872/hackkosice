<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Restaurant", inversedBy="categories")
     * @ORM\JoinColumn(nullable=false)
     */
    private $restaurantId;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\CategoryList", inversedBy="categories", fetch="EAGER")
     */
    private $categoryListId;

    public function __construct()
    {
        $this->categoryListId = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRestaurantId(): ?Restaurant
    {
        return $this->restaurantId;
    }

    public function setRestaurantId(?Restaurant $restaurantId): self
    {
        $this->restaurantId = $restaurantId;

        return $this;
    }

    /**
     * @return Collection|CategoryList[]
     */
    public function getCategoryListId(): Collection
    {
        return $this->categoryListId;
    }

    public function addCategoryListId(CategoryList $categoryListId): self
    {
        if (!$this->categoryListId->contains($categoryListId)) {
            $this->categoryListId[] = $categoryListId;
        }

        return $this;
    }

    public function removeCategoryListId(CategoryList $categoryListId): self
    {
        if ($this->categoryListId->contains($categoryListId)) {
            $this->categoryListId->removeElement($categoryListId);
        }

        return $this;
    }
}
