<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MeetingUsersRepository")
 */
class MeetingUsers
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Meeting", inversedBy="meetingUsers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $meetingId;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\MeetingCategoryList", mappedBy="MeetingUserId")
     */
    private $meetingCategoryList;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="meetingUsers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $userId;

    public function __construct()
    {
        $this->meetingCategoryList = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->userId->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getMeetingId(): ?Meeting
    {
        return $this->meetingId;
    }

    public function setMeetingId(?Meeting $meetingId): self
    {
        $this->meetingId = $meetingId;

        return $this;
    }

    /**
     * @return Collection|MeetingRestaurant[]
     */
    public function getMeetingCategoryList(): Collection
    {
        return $this->meetingCategoryList;
    }

    public function addMeetingRestaurant(MeetingRestaurant $meetingRestaurant): self
    {
        if (!$this->meetingCategoryList->contains($meetingRestaurant)) {
            $this->meetingCategoryList[] = $meetingRestaurant;
            $meetingRestaurant->addMeetingUserId($this);
        }

        return $this;
    }

    public function removeMeetingRestaurant(MeetingRestaurant $meetingRestaurant): self
    {
        if ($this->meetingCategoryList->contains($meetingRestaurant)) {
            $this->meetingCategoryList->removeElement($meetingRestaurant);
            $meetingRestaurant->removeMeetingUserId($this);
        }

        return $this;
    }

    public function getUserId(): ?User
    {
        return $this->userId;
    }

    public function setUserId(?User $userId): self
    {
        $this->userId = $userId;

        return $this;
    }
}
