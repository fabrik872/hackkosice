<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryListRepository")
 */
class CategoryList
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Category", mappedBy="categoryListId", fetch="EAGER")
     */
    private $categories;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\MeetingCategoryList", mappedBy="categoryListId")
     */
    private $meetingCategoryList;

    /**
     * @return Collection|CategoryList[]
     */
    public function getMeetingCategoryList()
    {
        return $this->meetingCategoryList;
    }

    public function setMeetingCategoryList($meetingCategoryList): void
    {
        $this->meetingCategoryList = $meetingCategoryList;
    }

    public function __construct()
    {
        $this->categories = new ArrayCollection();
    }

    public function __toString(): ?string
    {
        return $this->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
            $category->addCategoryListId($this);
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        if ($this->categories->contains($category)) {
            $this->categories->removeElement($category);
            $category->removeCategoryListId($this);
        }

        return $this;
    }
}
