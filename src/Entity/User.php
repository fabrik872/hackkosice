<?php

namespace App\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 */
class User implements UserInterface
{
    /**
     * @ORM\Id;
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $email;

    /**
     * @ORM\Column(type="string", length=40)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $role;

    /**
     * @Assert\Length(max=4096)
     */
    protected $plainPassword;

    /**
     * @ORM\Column(type="string", length=64)
     */
    protected $password;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\MeetingUsers", mappedBy="userId", orphanRemoval=true)
     */
    private $meetingUsers;

    public function __construct()
    {
        $this->meetingUsers = new ArrayCollection();
    }

    public function eraseCredentials()
    {
        return null;
    }

    public function getRole()
    {
        return $this->role;
    }

    public function setRole($role = null)
    {
        $this->role = $role;
    }

    public function getRoles()
    {
        return [$this->getRole()];
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getUsername()
    {
        return $this->email;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
    }

    public function getSalt()
    {
        return null;
    }

    /**
     * @return Collection|MeetingUsers[]
     */
    public function getMeetingUsers(): Collection
    {
        return $this->meetingUsers;
    }

    public function addMeetingUser(MeetingUsers $meetingUser): self
    {
        if (!$this->meetingUsers->contains($meetingUser)) {
            $this->meetingUsers[] = $meetingUser;
            $meetingUser->setUserId($this);
        }

        return $this;
    }

    public function removeMeetingUser(MeetingUsers $meetingUser): self
    {
        if ($this->meetingUsers->contains($meetingUser)) {
            $this->meetingUsers->removeElement($meetingUser);
            // set the owning side to null (unless already changed)
            if ($meetingUser->getUserId() === $this) {
                $meetingUser->setUserId(null);
            }
        }

        return $this;
    }
}