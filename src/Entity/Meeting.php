<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MeetingRepository")
 */
class Meeting
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\MeetingUsers", mappedBy="meetingId", orphanRemoval=true, fetch="EAGER")
     */
    private $meetingUsers;

    public function __construct()
    {
        $this->meetingUsers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return Collection|MeetingUsers[]
     */
    public function getMeetingUsers(): Collection
    {
        return $this->meetingUsers;
    }

    public function addMeetingUser(MeetingUsers $meetingUser): self
    {
        if (!$this->meetingUsers->contains($meetingUser)) {
            $this->meetingUsers[] = $meetingUser;
            $meetingUser->setMeetingId($this);
        }

        return $this;
    }

    public function removeMeetingUser(MeetingUsers $meetingUser): self
    {
        if ($this->meetingUsers->contains($meetingUser)) {
            $this->meetingUsers->removeElement($meetingUser);
            // set the owning side to null (unless already changed)
            if ($meetingUser->getMeetingId() === $this) {
                $meetingUser->setMeetingId(null);
            }
        }

        return $this;
    }
}
