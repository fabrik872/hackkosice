<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MeetingCategoryListRepository")
 */
class MeetingCategoryList
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\MeetingUsers", inversedBy="meetingCategoryList")
     */
    private $MeetingUserId;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\CategoryList", inversedBy="meetingCategoryList")
     */
    private $categoryListId;

    /**
     * @ORM\Column(type="integer")
     */
    private $ranking;

    public function __construct()
    {
        $this->MeetingUserId = new ArrayCollection();
        $this->categoryListId = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|MeetingUsers[]
     */
    public function getMeetingUserId(): Collection
    {
        return $this->MeetingUserId;
    }

    public function addMeetingUserId(MeetingUsers $meetingUserId): self
    {
        if (!$this->MeetingUserId->contains($meetingUserId)) {
            $this->MeetingUserId[] = $meetingUserId;
        }

        return $this;
    }

    public function removeMeetingUserId(MeetingUsers $meetingUserId): self
    {
        if ($this->MeetingUserId->contains($meetingUserId)) {
            $this->MeetingUserId->removeElement($meetingUserId);
        }

        return $this;
    }

    /**
     * @return Collection|Restaurant[]
     */
    public function getCategoryListId(): Collection
    {
        return $this->categoryListId;
    }

    public function addCategoryId(CategoryList $categoryId): self
    {
        if (!$this->categoryListId->contains($categoryId)) {
            $this->categoryListId[] = $categoryId;
        }

        return $this;
    }

    public function removeCategoryId(CategoryList $restaurantId): self
    {
        if ($this->categoryListId->contains($restaurantId)) {
            $this->categoryListId->removeElement($restaurantId);
        }

        return $this;
    }

    public function getRanking(): ?int
    {
        return $this->ranking;
    }

    public function setRanking(int $ranking): self
    {
        $this->ranking = $ranking;

        return $this;
    }
}
