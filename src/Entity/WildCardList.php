<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\WildCardListRepository")
 */
class WildCardList
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $weight;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\WildCard", mappedBy="wildCardListId", fetch="EAGER")
     */
    private $wildCards;

    public function __construct()
    {
        $this->wildCards = new ArrayCollection();
    }

    public function __toString(): ?string
    {
        return $this->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getWeight(): ?int
    {
        return $this->weight;
    }

    public function setWeight(int $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * @return Collection|WildCard[]
     */
    public function getWildCards(): Collection
    {
        return $this->wildCards;
    }

    public function addWildCard(WildCard $wildCard): self
    {
        if (!$this->wildCards->contains($wildCard)) {
            $this->wildCards[] = $wildCard;
            $wildCard->addWildCardListId($this);
        }

        return $this;
    }

    public function removeWildCard(WildCard $wildCard): self
    {
        if ($this->wildCards->contains($wildCard)) {
            $this->wildCards->removeElement($wildCard);
            $wildCard->removeWildCardListId($this);
        }

        return $this;
    }
}
