<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MeetingRestaurantRepository")
 */
class MeetingRestaurant
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\MeetingUsers", inversedBy="meetingRestaurants", fetch="EAGER")
     */
    private $MeetingUserId;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Restaurant", inversedBy="meetingRestaurants", fetch="EAGER")
     */
    private $restaurantId;

    public function __construct()
    {
        $this->MeetingUserId = new ArrayCollection();
        $this->restaurantId = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|MeetingUsers[]
     */
    public function getMeetingUserId(): Collection
    {
        return $this->MeetingUserId;
    }

    public function addMeetingUserId(MeetingUsers $meetingUserId): self
    {
        if (!$this->MeetingUserId->contains($meetingUserId)) {
            $this->MeetingUserId[] = $meetingUserId;
        }

        return $this;
    }

    public function removeMeetingUserId(MeetingUsers $meetingUserId): self
    {
        if ($this->MeetingUserId->contains($meetingUserId)) {
            $this->MeetingUserId->removeElement($meetingUserId);
        }

        return $this;
    }

    /**
     * @return Collection|Restaurant[]
     */
    public function getRestaurantId(): Collection
    {
        return $this->restaurantId;
    }

    public function addRestaurantId(Restaurant $restaurantId): self
    {
        if (!$this->restaurantId->contains($restaurantId)) {
            $this->restaurantId[] = $restaurantId;
        }

        return $this;
    }

    public function removeRestaurantId(Restaurant $restaurantId): self
    {
        if ($this->restaurantId->contains($restaurantId)) {
            $this->restaurantId->removeElement($restaurantId);
        }

        return $this;
    }
}
