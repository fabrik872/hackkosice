<?php

namespace App\Controller;

use App\Entity\WildCard;
use App\Form\WildCardType;
use App\Repository\WildCardRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/wild/card")
 */
class WildCardController extends Controller
{
    /**
     * @Route("/", name="wild_card_index", methods={"GET"})
     */
    public function index(WildCardRepository $wildCardRepository): Response
    {
        return $this->render('wild_card/index.html.twig', [
            'wild_cards' => $wildCardRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="wild_card_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $wildCard = new WildCard();
        $form = $this->createForm(WildCardType::class, $wildCard);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($wildCard);
            $entityManager->flush();

            return $this->redirectToRoute('wild_card_index');
        }

        return $this->render('wild_card/new.html.twig', [
            'wild_card' => $wildCard,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="wild_card_show", methods={"GET"})
     */
    public function show(WildCard $wildCard): Response
    {
        return $this->render('wild_card/show.html.twig', [
            'wild_card' => $wildCard,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="wild_card_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, WildCard $wildCard): Response
    {
        $form = $this->createForm(WildCardType::class, $wildCard);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('wild_card_index', [
                'id' => $wildCard->getId(),
            ]);
        }

        return $this->render('wild_card/edit.html.twig', [
            'wild_card' => $wildCard,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="wild_card_delete", methods={"DELETE"})
     */
    public function delete(Request $request, WildCard $wildCard): Response
    {
        if ($this->isCsrfTokenValid('delete'.$wildCard->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($wildCard);
            $entityManager->flush();
        }

        return $this->redirectToRoute('wild_card_index');
    }
}
