<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\CategoryList;
use App\Entity\Meeting;
use App\Entity\MeetingCategoryList;
use App\Entity\MeetingUsers;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MeetingController extends Controller
{
    /**
     * @Route("/create_meeting", name="create_meeting")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();
        $meeting = new Meeting();
        $meeting->setDate(new \DateTime());
        $em->persist($meeting);
        $em->flush();
        /**
         * @var Meeting $meeting
         */
        $meeting = $em->getRepository(Meeting::class)->findLast();
        $meetingUser = new MeetingUsers();
        $meetingUser->setMeetingId($meeting);
        $meetingUser->setUserId($this->getUser());
        dump($meeting->addMeetingUser($meetingUser));

        $categories = $em->getRepository(CategoryList::class)->findAll();
        dump($categories);

        return $this->render('meeting/create.html.twig', [
            'meeting_id' => $meeting->getId(),
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/add_member/{id}", name="add_member")
     */
    public function addUser($id)
    {
        $em = $this->getDoctrine()->getManager();

        $meeting = $em->getRepository(Meeting::class)->find($id);
        $meetingUser = new MeetingUsers();
        $meetingUser->setMeetingId($meeting);
        $meetingUser->setUserId($this->getUser());
        dump($meeting->addMeetingUser($meetingUser));

        $categories = $em->getRepository(CategoryList::class)->findAll();
        dump($categories);

        return $this->render('meeting/category_ranking.html.twig', [
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/rank_categories/{id}", name="rank_categories")
     */
    public function rankCategories(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $meetingUsers = new MeetingUsers();

        /**
         * @var Meeting $meeting
         */
        $meetingUsers->setUserId($this->getUser());
        $meeting = $em->getRepository(Meeting::class)->find($id);
        $meetingUsers->setMeetingId($meeting);

        $meetingCategoryList = new MeetingCategoryList();
        $meetingCategoryList->addMeetingUserId($meetingUsers);

        foreach ($request->get('category') as $key => $value) {
            $categoryList = $em->getRepository(CategoryList::class)->find($key);
            /**
             * @var CategoryList $categoryList
             */
            dump($categoryList);
            $meetingCategoryList->addCategoryId($categoryList);
            $meetingCategoryList->setRanking($value);
        }

        $em->persist($meetingUsers);
        $em->persist($meetingCategoryList);
        $em->flush();

        dump($meetingUsers);
        dump($request->get('category'));
        die();
    }
}
