<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        return $this->render('public/index.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }

    public function createUser()
    {

    }
}
