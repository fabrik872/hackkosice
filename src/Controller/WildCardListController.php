<?php

namespace App\Controller;

use App\Entity\WildCardList;
use App\Form\WildCardListType;
use App\Repository\WildCardListRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/wild/card/list")
 */
class WildCardListController extends Controller
{
    /**
     * @Route("/", name="wild_card_list_index", methods={"GET"})
     */
    public function index(WildCardListRepository $wildCardListRepository): Response
    {
        return $this->render('wild_card_list/index.html.twig', [
            'wild_card_lists' => $wildCardListRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="wild_card_list_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $wildCardList = new WildCardList();
        $form = $this->createForm(WildCardListType::class, $wildCardList);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($wildCardList);
            $entityManager->flush();

            return $this->redirectToRoute('wild_card_list_index');
        }

        return $this->render('wild_card_list/new.html.twig', [
            'wild_card_list' => $wildCardList,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="wild_card_list_show", methods={"GET"})
     */
    public function show(WildCardList $wildCardList): Response
    {
        return $this->render('wild_card_list/show.html.twig', [
            'wild_card_list' => $wildCardList,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="wild_card_list_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, WildCardList $wildCardList): Response
    {
        $form = $this->createForm(WildCardListType::class, $wildCardList);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('wild_card_list_index', [
                'id' => $wildCardList->getId(),
            ]);
        }

        return $this->render('wild_card_list/edit.html.twig', [
            'wild_card_list' => $wildCardList,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="wild_card_list_delete", methods={"DELETE"})
     */
    public function delete(Request $request, WildCardList $wildCardList): Response
    {
        if ($this->isCsrfTokenValid('delete'.$wildCardList->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($wildCardList);
            $entityManager->flush();
        }

        return $this->redirectToRoute('wild_card_list_index');
    }
}
