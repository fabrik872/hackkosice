<?php

namespace App\Controller;

use App\Entity\CategoryList;
use App\Form\CategoryListType;
use App\Repository\CategoryListRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/category_list")
 */
class CategoryListController extends Controller
{
    /**
     * @Route("/", name="category_list_index", methods={"GET"})
     */
    public function index(CategoryListRepository $categoryListRepository): Response
    {
        return $this->render('category_list/index.html.twig', [
            'category_lists' => $categoryListRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="category_list_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $categoryList = new CategoryList();
        $form = $this->createForm(CategoryListType::class, $categoryList);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($categoryList);
            $entityManager->flush();

            return $this->redirectToRoute('category_list_index');
        }

        return $this->render('category_list/new.html.twig', [
            'category_list' => $categoryList,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="category_list_show", methods={"GET"})
     */
    public function show(CategoryList $categoryList): Response
    {
        return $this->render('category_list/show.html.twig', [
            'category_list' => $categoryList,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="category_list_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, CategoryList $categoryList): Response
    {
        $form = $this->createForm(CategoryListType::class, $categoryList);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('category_list_index', [
                'id' => $categoryList->getId(),
            ]);
        }

        return $this->render('category_list/edit.html.twig', [
            'category_list' => $categoryList,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="category_list_delete", methods={"DELETE"})
     */
    public function delete(Request $request, CategoryList $categoryList): Response
    {
        if ($this->isCsrfTokenValid('delete'.$categoryList->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($categoryList);
            $entityManager->flush();
        }

        return $this->redirectToRoute('category_list_index');
    }
}
